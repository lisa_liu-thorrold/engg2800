package midiblocks;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Timer;
import java.util.TimerTask;

import processingblocks.Arpeggiator;
import processingblocks.Gates;
import processingblocks.ProcessingBlock;
import scales.Note;

/**
 * This class is responsible for  processing all the events received,
 * through the processing blocks, and then outputting them to the 
 * relevant output devices. This class is a subject that observes
 * MIDI input events, and is observed by the MIDI output class.
 * @author Lisa
 *
 */
public class ProcessingBlockController implements Subject, Observer {
	
	// Notes available in the currently selected scale
	private Note[] availableNotes;
	
	// Observers listening to events from this class (OutputProcessor)
	private final ArrayList<Observer> observers = new ArrayList<>();

	private final MidiModel midiModel;

	// The list of processing blocks
	private final LinkedList<ProcessingBlock> processingBlocks;
	
	private boolean timerStarted;
	
	// Timer to alert any arpeggiator/ gate processing blocks to do their
	// required functions
	private ProcessingBlockTimer timer;
	
	// To observe the last chain in the processing block
	private final ProcessingBlockChainObserver pblockChainObserver;
	
	/**
	 * This class is responsible for handling the inputs received the the
	 * input source, sending it through the processing blocks in the chain,
	 * and sending it on to the relevant observers for midi output
	 * @param midiModel - The MIDI model which stores state about this program
	 */
	public ProcessingBlockController(MidiModel midiModel) {
		this.midiModel = midiModel;
		this.processingBlocks = midiModel.processingBlocks;
		midiModel.addListener("newTempo", event -> tempoUpdate());
		pblockChainObserver = new ProcessingBlockChainObserver();
		connectProcessingBlocks();
	}
	
	/**
	 * This is notified when we get a change in midi source.
	 * It should release all the notes stored in the processing
	 * block's buffer (gates/ arpeggiator) so the notes do not
	 * keep repeating after input has stopped.
	 */
	public void changeMidiSource() {
		for (ProcessingBlock processingBlock : processingBlocks) {
			if(processingBlock instanceof Arpeggiator) {
				((Arpeggiator)processingBlock).clearNotesCurrentlyOn();
			}
			
			if (processingBlock instanceof Gates) {
				((Gates)processingBlock).clearGateQueue();
			}
		}
	}
	

	/**
	 * Get notified when we receive a new note. Start the timer if it hasn't
	 * started. Processes this note and sends it to the first observer
	 * @param note - The note received by the processing block controller
	 * @param noteOn - Whether the note is on or not
	 */
	@Override
	public void update(Note note, Boolean noteOn) {

		if (!timerStarted) {
			System.out.println("Timer started false, ");
			timer = new ProcessingBlockTimer();
			int beat = 60000/(midiModel.getTempo());
			timer.schedule(beat);
			timerStarted = true;
		}
		
		 processNote(note, noteOn);
	}

	/**
	 * Need to get through list of processing blocks, and process the input
	 * through it.
	 * @param note - The note to process through the processing blocks
	 * @param noteOn - Whether the note is a note on message
	 */
	private void processNote(Note note, Boolean noteOn) {

		// if there are processing blocks
		if (processingBlocks.size() > 0) {

			// Only need to send the message to the first processing block,
			// it will send messages to the following processing blocks in a
			// domino chain like effect
			processingBlocks.getFirst().update(note, noteOn, availableNotes);

		} else {

			// no processing blocks, send straight to output
			notifyObservers(note, noteOn, availableNotes);
		}
		
	}
	
	/**
	 * This method changes the processing blocks timing, when the tempo
	 * is updated by the user.
	 */
	private void tempoUpdate() {
		if (timerStarted) {
			int beat = 60000/(midiModel.getTempo());
			timer.reschedule(beat);
		}
	}
	
	/**
	 * Connects the processing blocks so that each processing block listens for
	 * output from the processing block above it in the chain. 
	 */
	public void connectProcessingBlocks() {
		
		// should flush output buffer 
		flushOutput();
		
		System.out.println("Reconnecting the processing blocks");
		
		/* Remove all observers currently on the processing blocks */
		processingBlocks.forEach(processingblocks.ProcessingBlock::removeAllObservers);
		
		/* Connect all the processing blocks */
		for (int i = 0; i < processingBlocks.size() -1; i++) {
			processingBlocks.get(i).setAvailableNotes(availableNotes);
			System.out.println("Register the " + (i+1) + "th processing block as an"
					+ " observer to the " + i + "th processing block");
			processingBlocks.get(i).registerObserver(processingBlocks.get(i+1));
		}

		if (processingBlocks.size() > 0) {
			//register the last processing block.
			processingBlocks.get(processingBlocks.size()-1).registerObserver(
					pblockChainObserver);
			System.out.println("Register the last observer to the " + (processingBlocks.size()-1) + " processing block");
		}
	}
	
	/**
	 * Flushes output messages so no note on's are left hanging due to the
	 * change in the configurations of the processing blocks. This shouldn't 
	 * really be a problem unless we are processing midi files as inputs.
	 */
	private void flushOutput() {
		observers.stream().filter(ob -> ob instanceof OutputProcessor).forEach(
				ob -> ((OutputProcessor) ob).flushMessages());
	}
	
	/*************************************************
	 *  Subject methods
	 *************************************************/
	
	/**
	 * Register the output processor to listen to events from this class
	 * @param observer - observer for events from this class
	 */
	@Override
	public void registerObserver(Observer observer) {
		observers.add(observer);	
	}

	/**
	 * Stop the output processor from listening to events from this class
	 * (so that the output processor does not receive duplicate events)
	 */
	@Override
	public void removeObserver(Observer observer) {
		observers.remove(observer);	
	}
	
	/**
	 * Send the processed midi events to the output processor
	 */
	private void notifyObservers(Note note, Boolean noteOn, Note[] availableNotes) {
		System.out.println("Message sending to output: ");
		System.out.println("Note: " + note + "(" + note.getKeyNumber() + ") " + ", Note on?: " + noteOn);

		for (Observer ob : observers) {
			ob.update(note, noteOn);
		}	
	}

	@Override
	public void update(Note note, Boolean noteOn, Note[] availableNotes) { }
	
	@Override
	public void notifyObservers(Note note, Boolean noteOn) { }
	
	/**
	 * Stop the output processor from listening to events from this class
	 * (so that the output processor does not receive duplicate events)
	 */
	public void removeAllObservers() {
		Iterator<Observer> iterator = observers.iterator();

		while (iterator.hasNext()){
			iterator.next();
			iterator.remove();
		}
	}

	/*************************************************
	 *  Setter methods
	 *************************************************/

	public void setAvailableNotes() {
		this.availableNotes = midiModel.getAvailableNotes();
	}
	
	/*************************************************
	 *  Private Inner classes to support timing 
	 *************************************************/
	
	/**
	 * This class is the timer responsible for handling processing blocks that
	 * depend on timing (gates / arpeggiator)
	 * @author Lisa
	 *
	 */
	private class ProcessingBlockTimerTask extends TimerTask {
		
		/**
		 * On every tick, this method runs arpeggiate / gate release function
		 */
		public void run() {

			for (ProcessingBlock processingBlock : processingBlocks) {
				if (processingBlock instanceof Arpeggiator) {
					// cast to arpeggiator
					// arpeggiate
					Arpeggiator arpBlock = (Arpeggiator) processingBlock;
					arpBlock.arpeggiate();

				}

				if (processingBlock instanceof Gates) {
					// cast to gates
					Gates gateBlock = (Gates) processingBlock;
					gateBlock.release();
				}
			}
			
		}
	}
	
	/**
	 * This class is response for initialising the timer task, and handling
	 * rescheduling when the tempo is changed by the users.
	 * @author Lisa
	 *
	 */
	private class ProcessingBlockTimer extends Timer {
		Timer timer;
		final ProcessingBlockTimerTask task = new ProcessingBlockTimerTask();
		TimerTask timerTask;
		
		
		  public void schedule(long delay) {
			  timer = new Timer();

			  timerTask = new TimerTask() { 
				  public void run() { task.run(); }
			  };
			  
			  timer.schedule(timerTask, 0, delay);
		  }
		   
		  public void reschedule(long delay) {
		    timerTask.cancel();
		    timerTask = new TimerTask() { 
		    	public void run() { task.run(); }
			};
		    
		    timer.schedule(timerTask, 0, delay);
		  }
	}
	
	/**
	 * This class is responsible to for observing the last processing block
	 * in the processing block chain for events. This class it not used if
	 * there are no processing blocks to observe.
	 * @author Lisa
	 *
	 */
	private class ProcessingBlockChainObserver implements Observer {

		public void update(Note note, Boolean noteOn, Note[] availableNotes) {
			// Sends the midi events to the output processor received from the
			// last processing block in the chain.
			notifyObservers(note, noteOn, availableNotes);
		}

		@Override
		public void update(Note note, Boolean noteOn) { }
		
	}

}
