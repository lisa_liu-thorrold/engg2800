package processingblocks;

/**
 * This class uses the Factory Design Pattern to make a processing block.
 * @author Lisa
 *
 */
public class ProcessingBlockFactory {
	
	private final static String ARPEGGIATOR = "ARPEGGIATOR";
	private final static String CHORDIFY = "CHORDIFY";
	private final static String GATES = "GATES";
	private final static String MONOPHONIC = "MONOPHONIC";
	private final static String PITCH_SHIFT = "PITCHSHIFT";
	
	   public ProcessingBlock makeProcessingBlock(String name, 
			   String parameter1, String parameter2){
	
	      if(name.equalsIgnoreCase(ARPEGGIATOR)){
	         return new Arpeggiator(parameter1);
	      } else if(name.equalsIgnoreCase(CHORDIFY)){
	         return new Chordify();
	      } else if(name.equalsIgnoreCase(GATES)) {
	    	  double notesPerTick = Double.parseDouble(parameter2);
	    	  if(notesPerTick == 0) { return null; }
	    	  return new Gates(parameter1, notesPerTick);
	      } else if(name.equalsIgnoreCase(MONOPHONIC)){
	         return new Monophonic();
	      } else if(name.equalsIgnoreCase(PITCH_SHIFT)){
	    	  int pitch = Integer.parseInt(parameter1);
		         return new PitchShift(pitch);
		  } 
	      
	      return null;
	   }
}



