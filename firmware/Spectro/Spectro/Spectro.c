/*
 * Spectro.c
 *
 * Created: 24/10/2015 5:38:33 AM
 *  Author: Luca
 */ 


#include <avr/io.h>

int main(void)
{
	//1->8 are PORT D, 9 is PB0, 10 is PB1
	//Row 1 is the bottom row
	DDRD = 0xFF;
	DDRB = (1<<0)|(1<<1);
	
	//1->4 are PORTC (0->3)
	DDRC = 0x0F;
	
	//Row 10 on, all others off
	PORTD = 0x00;
	PORTB &= ~(0x03);
	PORTB |= (1<<1);
	
	//Col 1 off, all others on
	PORTC = (1<<3)|(1<<2)|(1<<1);
	
    while(1)
    {
        //TODO:: Please write your application code 
    }
}