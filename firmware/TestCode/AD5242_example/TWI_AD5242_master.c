/*****************************************************************************
*                                    TWI_AD5242_master.c
*
****************************************************************************/
#include <avr/interrupt.h>
#include <avr/io.h>
#include "TWI_AD5242_master.h"




/*************************************************************************
  Issues a start condition and sends address to I2C device
  
  args:    AD1/AD0 combination selects your target I2C device. 
               OPTIONS: LOW or HIGH
            R_W_ specifies a read or write to AD5242 device. 
			   OPTIONS: LOW ->Write, HIGH ->Read

  Return:   0 write successful 
            1 write failed
*************************************************************************/
unsigned char i2c_write_frame_1( uint8_t AD1_,uint8_t AD0_,uint8_t R_W_  )
{	
  
    
	uint8_t  addr1 = 0x58; //(0x01011000)
 
     // set I2C device selection
	addr1 |= (AD1_ << AD1);
	addr1 |= (AD0_ << AD0);

	//  set selected I2C device to read or write
    addr1 |= (R_W_ << R_W);

   uint8_t   twst;

	// send START condition
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

	// wait until transmission completed
	while(!(TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_START) && (twst != TW_REP_START)) return 1;

	// send device address
	TWDR = addr1;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wail until transmission completed and ACK/NACK has been received
	while(!(TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) ) return 1;

	return 0;

}/* i2c_write_frame_1 */

/*************************************************************************
  Issues a start condition and sends address to I2C device
  
  args:    RDAC selects your internal  resistor device. 
               OPTIONS: LOW -> RDAC1, or HIGH ->RDAC2

           output_1 , enables output 1  
			   OPTIONS: LOW ->disable, HIGH ->enable

           output_2 , enables output 2 
			   OPTIONS: LOW ->disable, HIGH ->enable

  Return:   0 write successful 
            1 write failed
*************************************************************************/
unsigned char i2c_write_frame_2( uint8_t output_1,uint8_t output_2,uint8_t RDAC )
{	
    uint8_t   twst;
    uint8_t   addr1 = 0x00;

     // set output 1, output 2 bit
	addr1 |= (output_1 << OUTPUT_1);
	addr1 |= (output_2 << OUTPUT_2);

	//  select either RDAC1 or RDAC2
    addr1 |= (RDAC << A_B);


	// send data to the previously addressed device
	TWDR = addr1;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission completed
	while(!(TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits
	twst = TW_STATUS & 0xF8;
	if( twst != TW_MT_DATA_ACK) return 1;
	return 0;

}/* i2c_write_frame_2 */


/*************************************************************************
  Send one byte to I2C device
  
  arg:    byte to be transfered. hex value 0 -> 0xFF (up to 255 values allowed)
  Return:   0 write successful 
            1 write failed
*************************************************************************/
unsigned char i2c_write_frame_3( uint8_t data )
{	
    uint8_t   twst;
    
	// send data to the previously addressed device
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission completed
	while(!(TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits
	twst = TW_STATUS & 0xF8;
	if( twst != TW_MT_DATA_ACK) return 1;
	return 0;

}/* i2c_write_frame_3 */

/*************************************************************************	
  Issues a start condition and sends address and transfer direction.
  return 0 = device accessible, 1= failed to access device
*************************************************************************/
unsigned char i2c_start(unsigned char address)
{
    uint8_t   twst;

	// send START condition
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN);

	// wait until transmission completed
	while(!(TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_START) && (twst != TW_REP_START)) return 1;

	// send device address
	TWDR = address;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wail until transmission completed and ACK/NACK has been received
	while(!(TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits.
	twst = TW_STATUS & 0xF8;
	if ( (twst != TW_MT_SLA_ACK) && (twst != TW_MR_SLA_ACK) ) return 1;

	return 0;

}/* i2c_start */

/* 
*


*************************************************************************
  Send one byte to I2C device
  
  Input:    byte to be transfered
  Return:   0 write successful 
            1 write failed
*************************************************************************/
unsigned char i2c_write( unsigned char data )
{	
    uint8_t   twst;
    
	// send data to the previously addressed device
	TWDR = data;
	TWCR = (1<<TWINT) | (1<<TWEN);

	// wait until transmission completed
	while(!(TWCR & (1<<TWINT)));

	// check value of TWI Status Register. Mask prescaler bits
	twst = TW_STATUS & 0xF8;
	if( twst != TW_MT_DATA_ACK) return 1;
	return 0;

}/* i2c_write */

/*************************************************************************
 Initialization of the I2C bus interface. Need to be called only once

 					USED TO INITIALIZE TWI CLOCK SPEED
 SEE FORMULA (pg223 ATMEGA328P datasheet) FOR PROPER SETTING OF TWBR REGISTER!!
 TWI CLOCK SPEED DEPENDS ON CPU CLOCK, PRESCALER VALUE AND TWBR VALUE
*************************************************************************/
void i2c_init(void)
{
  /* initialize TWI clock: 25 kHz clock, TWPS = 0 => prescaler = 1 */
  // See pg 223 of datasheet for formula
  //Tricky to set up !!

  TWSR = 0;                         /*  prescaler 1*/
  TWBR = 0x0C; //  /* must be > 10 for stable operation */



}/* i2c_init */


/*************************************************************************
 Terminates the data transfer and releases the I2C bus
*************************************************************************/
void i2c_stop(void)
{
    /* send stop condition */
	TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWSTO);
	
	// wait until stop condition is executed and bus released
	while(TWCR & (1<<TWSTO));

}/* i2c_stop */


