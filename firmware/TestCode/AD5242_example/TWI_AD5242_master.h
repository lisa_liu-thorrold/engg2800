/*****************************************************************************
* 								TWI_AD5242_master.h
*                             AD5242 library, with any AVR MEGA with TWI support
* Date: 11/17/2011
* Original Author: Peter Fleury
* Modified by: Mario Rivas
* Limitations: 
 	1) No read
	2) No shut down
	3) No midscale reset 
	All above limitations can be overcome with minor code modifications.
****************************************************************************/

//Requires 1MHz CPU for TWI 25,000Hz clock.
// datasheet: http://www.analog.com/static/imported-files/data_sheets/AD5241_5242.pdf


/*    bit no.    76543  2   1   0         7  6  5  4  3  2 1 0          8 bit
                 01011 AD1 AD0 R/W __A__ A/B RS SD O1 O2 X X X  __A__ DATA-BYTE
*				      FRAME 1                    FRAME 2                FRAME 3
*
*           0) Addressing your IC is done in the first frame of the control register.
*			1) The AD5242 can be paired with other ICs  using the AD0/AD1 combination bits.
*	           One IC can have '00','01','11', or '10' logic bits set. Therefore, 4 total possible combinations can co-exist.
*				   NOTE: In order to set a AD0/AD1 bit HIGH (a logic '1') for a particular IC, you 
*				   would apply VCC to that particular AD0/AD1 pin. Likewise, to set a AD0/AD1 bit LOW (a logic '0')
*				   you would ground the pin. In AD5242, AD0 is pin 9. AD1 is pin 10.
*				
*           2) TWI clock (i2c_init()) depends on several factors. Please see formula on atmega datasheet. Default 25Khz uses 1Mhz cpu clock
*			   
*			3) The absolute highest voltage allowed to pins A,B,W is 7V. Voltages higher than VCC will cause damage.			  
*
*     		4) Resistors used to pull up SDA and SCL are 21k, however, 5k might be need for faster I2C speeds.
*
*    		5) The AD5242/AD5241 has a control register  composed of three frames.
*			    FRAME 1: Used for adressing your particular IC regardless of configuration. For example, if you have only one IC 
*				         with pin 9 and pin 10 set LOW, then you would address your particular IC by writing '00'
*						 to bits 1 and 2 of FRAME 1. In other words, FRAME 1 would be '01011000'. Also, FRAME 1 contains bit 0 to 
*						 specify your command. Bit 0 (R/W) indicates wheter you want to Write to the Register or Read. Since you
*						 will mostly be writing to the register in order to specify your resistor values, you want bit 0 LOW.
*
*               FRAME 2: Used to select your RDAC. AD5242 has two RDACs (programable resistors). You program bit 7 to address your 
*				         RDAC. For example, you would set bit 7 LOW  to address RDAC1 OR HIGH for RDAC2. Because bit 7 of frame two, 
*              			 be set HIGH or LOW (logic '1' or '0'), you can not program the two resistors ("RDAC1 and RDAC2" in the AD5242)  
*			   			 simultaneously. In other words, you have to start a new start condition  everytime you want to address a 
*						 particular resistor (RDAC). Notice that FRAME 2 also enables or disables your two output pins (O1/O2). 
*						 These pins (pin 1/ pin 13) can source enough amperage to drive a N/P Fet or other logic gate. Writing a 
*						 LOW  to bits 3 or 4 disables the pin. Writing a HIGH enables the pin (outsources amperage). Note: you can address  
*				         both output pins simultaneously.
*				
*				FRAME 3: Used to specify your data. Only hex values from 0 -255 are allowed, corresponding resistance from 68OHM - 1M 
*                        OHM (In AD5242) between pins W/B/A.
*

AD5242 Configuration
Target AVR: any Mega with TWI supported (in this example, Atmega328P)

Pin											Pin
1 O1										9	GRND			
2 RDAC1,A									10  GRND
3 RDAC1,Wiper								11  GRND
4 RDAC1,B									12  GRND
5 VCC										13  02
6 GRND										14  RDAC2,B
7 SCL (to uC with 21k Resistor to VCC)		15  RDAC2,Wiper
8 SDA (to uC with 21k Resistor to VCC)		16  RDAC2,A

*/



#include <util/twi.h>

#define HIGH 0x01
#define LOW 0x00

// FRAME 1 bit masks
#define AD1 0x02
#define AD0 0x01
#define R_W 0x00

// FRAME 2 bit masks
#define OUTPUT_1 0x04
#define OUTPUT_2 0x03
#define A_B      0x07

unsigned char i2c_write_frame_1 (uint8_t,uint8_t,uint8_t);
unsigned char i2c_write_frame_2 (uint8_t,uint8_t,uint8_t);
unsigned char i2c_write_frame_3 ( uint8_t );


void i2c_init(void);
unsigned char i2c_start(unsigned char);
unsigned char i2c_write( unsigned char );
void i2c_stop(void);
