

#define F_CPU 1000000UL


#include "TWI_AD5242_master.h"


#include <util/delay.h>
#include <stdio.h>
#include <avr/io.h>
#include <stdlib.h>




int main (void){

i2c_init(); //called only once


i2c_write_frame_1 (LOW,LOW,LOW); //Initiate Start and select I2C device,,,, pg 5 of AD5242 datasheet
								 // AD0 and AD1 is set low, Write selected
i2c_write_frame_2(LOW,LOW,LOW);  // disable output 1, disable output 2, RDAC 1 selected,
i2c_write_frame_3(0x7C); 				// RDAC value = 128  - new value for variable resistor. Range 0..0xff
i2c_stop();

  while(1){
  
  _delay_ms(2500);
  i2c_write_frame_1 (LOW,LOW,LOW);
  i2c_write_frame_2(HIGH,LOW,HIGH);   	//enable output 1, disable output 2, RDAC 2 selected
  i2c_write_frame_3(0x2C);				// RDAC2 value 44 for variable resistor
  i2c_stop();

  _delay_ms(2500);
  i2c_write_frame_1 (LOW,LOW,LOW);
  i2c_write_frame_2(LOW,HIGH,LOW);   	//disable output 1, enable output 2, RDAC 1 selected
  i2c_write_frame_3(0xC8);				// RDAC1 value 200 for variable resistor
  i2c_stop();

  _delay_ms(2500);
  i2c_write_frame_1 (LOW,LOW,LOW);
  i2c_write_frame_2(LOW,LOW,HIGH);   	//disable output 1, disable output 2, RDAC 2 selected
  i2c_write_frame_3(0x7C);				// RDAC2 value 128 for variable resistor
  i2c_stop();
 
   _delay_ms(2500);
  i2c_write_frame_1 (LOW,LOW,LOW);
  i2c_write_frame_2(HIGH,HIGH,LOW);   	//enable output 1, enable output 2, RDAC 1 selected
  i2c_write_frame_3(0x1C);				// RDAC1 value 28  for variable resistor
  i2c_stop();


  
 ; }


}
