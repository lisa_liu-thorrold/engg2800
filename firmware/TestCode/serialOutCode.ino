

void setup() {
  // put your setup code here, to run once:
  
  SPI_MasterInit();
  SPI_MasterTransmit(0b10011001);
}

void loop() {
  // put your main code here, to run repeatedly:

}

void SPI_MasterInit(void)
{
  /* Set MOSI and SCK output, all others input */
  //DDR_SPI = (1<<DD_SS)|(1<<DD_MOSI)|(1<<DD_SCK);
  DDRB = (1<<2)|(1<<3)|(1<<5);
  /* Enable SPI, Master, set clock rate fck/16 */
  SPCR = (1<<SPE)|(1<<MSTR)|(1<<SPR0);
}

void SPI_MasterTransmit(char cData)
{
  /* Start transmission */
  // pull latch low
  PORTB &= ~(1<<2);
  SPDR = cData;
  /* Wait for transmission complete */
  while(!(SPSR & (1<<SPIF)))
    ;
  // pull latch high
  PORTB |= (1<<2);
}
